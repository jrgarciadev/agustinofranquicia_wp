<?php
/**
 * Archivo principal
 *
 * Archivo donde residen el resto de partes del tema, se puede describir
 * como el archivo padre o principal
 *
 * @package WordPress
 * @subpackage Franquicia 
 * @since Franquicia 1.0
 * @author Junior Garcia <www.jrdeveloper.com.ve> jrgarciadev@gmail.com
 */
get_header(); ?>

	<?php get_sidebar();?>

	<?php get_footer();?>

</body>

</html>