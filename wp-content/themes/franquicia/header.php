<?php
/**
 * Header del tema
 *
 * Muestra elementos comunes para casi todo el tema
 *
 * @package WordPress
 * @subpackage Franquicia
 * @since Franquicia 1.0
 * @author Junior Garcia <www.jrdeveloper.com.ve> jrgarciadev@gmail.com
 */
?>
<!DOCTYPE html>
<html <?php language_attributes();?> class="no-js">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php bloginfo('name');?></title>
	<link rel="stylesheet" href="<?php bloginfo('template_url')?>/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url')?>/fonts/font-awesome.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url')?>/fonts/material-icons.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url')?>/fonts/simple-line-icons.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url')?>/fonts/ionicons.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url')?>/fonts/ionicons.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url')?>/css/animate.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i">
	<!-- In <head> after the Bootstrap CSS. -->
		<link rel="stylesheet" href="https://unpkg.com/@bootstrapstudio/bootstrap-better-nav/dist/bootstrap-better-nav.min.css">

		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url')?>">
		<?php wp_head();?>
	</head>

	<body <?php body_class();?> style="font-family:'Noto Sans', sans-serif;">