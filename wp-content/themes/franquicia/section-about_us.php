    <section id="about_us">
        <div id="about_us_div" class="secondary_color_block">
            <div class="container-fluid">
                <div id="about_us_img"><img src="<?php bloginfo('template_url')?>/img/AgustinoCL-Quienes-Somos.gif" class="img-fluid"></div>
                <div id="about_us_2_decoration2" class="d-inline pull-right"></div>
                <div class="block_primary_text">

                    <?php if (have_posts()): while (have_posts()): the_post();?>
		                      <?php if (get_post_field('post_name', get_post()) == 'slug-1'): ?>
		                          <p class='text_primary top_primary_text'><?php the_title();?></p>
		                          <?php the_content();?>
		                      <?php endif;?>
	                  <?php endwhile;endif;?>

                  <a class="btn btn-primary primary_button" role="button" id="about_us_button"
                  href='#contact_form_div'>| TIENDA ONLINE</a></div>
                  <div id="about_us_2_decoration" class="square_decoration"></div>
              </div>
          </div>
          <div id="about_us_div_2">
            <div class="container">
                <div class="row d-flex align-items-start">
                    <div class="col-12 col-lg-5 col-md-7" id="about_2_img_col">
                        <div id="slide_1">
                            <img src="<?php bloginfo('template_url')?>/img/AgustinoCL-Nuestra-Mirada.gif" class="border_white img-fluid slide_1_img">
                            <img src="<?php bloginfo('template_url')?>/img/AgustinoCL-Nuestra-Mirada.gif" class="border_white img-fluid slide_1_img">
                            <img src="<?php bloginfo('template_url')?>/img/AgustinoCL-Nuestra-Mirada.gif" class="border_white img-fluid slide_1_img">
                            <img src="<?php bloginfo('template_url')?>/img/AgustinoCL-Nuestra-Mirada.gif" class="border_white img-fluid slide_1_img">
                            <a href="#" id = "slide_1_back" class=" slidesjs-previous  slidesjs-navigation btn btn-primary default_button"><i class="icon ion-ios-arrow-back"></i></a>
                            <a href="#" id = "slide_1_next" class="slidesjs-next slidesjs-navigation btn btn-primary default_button"><i class="icon ion-ios-arrow-forward"></i></a>

                        </div>
                    </div>


                    <div class="col-12 col-lg-7 col-md-5" id="col_about_us_text">
                        <div id="about_us_2_text">

                            <?php if (have_posts()): while (have_posts()): the_post();?>
		                              <?php if (get_post_field('post_name', get_post()) == 'slug-2'): ?>
		                                  <p class='text_default text_secondary_title'><?php the_title();?></p>
		                                  <?php the_content();?>
		                              <?php endif;?>
	                          <?php endwhile;endif;?>
                          <a class="btn btn-primary default_button"
                          role="button" id="contact_button"
                          href='#contact_form_div'>|&nbsp;Contáctanos</a></div>
                      </div>
                  </div>
              </div>
          </div>
      </section>