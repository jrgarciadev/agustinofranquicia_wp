    <section id="agustino_shop">
        <div id="agustino_shop_div1">
            <div class="container">
                <div class="row" id="agustino_row_1">
                    <div class="col col-lg-5 col-md-5" id="agustino_shop_img1_col">
                        <div id="agustino_shop_img1_div"><img src="<?php bloginfo('template_url')?>/img/AgustinoCL-Franquicia-1.gif" id="agustino_shop_img1" class="border_white img-fluid"></div>
                    </div>
                    <div class="col col-lg-7 pt-lg-5 col-md-5">
                        <div id="agustino_shop_1_text">

                            <?php if (have_posts()): while (have_posts()): the_post();?>
		                                <?php if (get_post_field('post_name', get_post()) == 'slug-3'): ?>
		                                    <p class='text_default text_secondary_title'><?php the_title();?></p>
		                                    <?php the_content();?>
		                                <?php endif;?>
	                            <?php endwhile;endif;?>

                            <a class="btn btn-primary default_button" href='#contact_form_div' role="button">| &nbsp;Trabajemos juntos</a></div>
                        </div>
                        <div class="col">
                            <div id="agustino_shop_img2" class="d-flex justify-content-center align-items-center"><img src="<?php bloginfo('template_url')?>/img/AgustinoCL-Franquicia-2.gif" id="agustino_img2_middle" class="border_white img-fluid"></div>
                        </div>
                    </div>
                    <div class="row" id="agustino_row_2">
                        <div class="col pt-lg-3 col-lg-7 col-md-7">
                            <div class="text_block_with_list">


                                <?php if (have_posts()): while (have_posts()): the_post();?>
		                                    <?php if (get_post_field('post_name', get_post()) == 'slug-4'): ?>
		                                     <h1 class="text_secondary heading_secondary_2 none_margin"><?php the_title();?></h1>
		                                     <?php the_content();?>
		                                 <?php endif;?>
	                             <?php endwhile;endif;?>
                             <div class="d-flex justify-content-center align-items-center"><a href="#"><i class="fa fa-angle-down down_arrow_icon icon-arrow-down" data-bs-hover-animate="rubberBand" id="down_arrow_icon2"></i></a></div>
                         </div>
                     </div>
                     <div class="col col-lg-5 col-md-5 block_img_md_right">
                        <div id="agustino_shop_2_img"><img src="<?php bloginfo('template_url')?>/img/AgustinoCL-Franquicia-3.gif" class="border_white img-fluid block_img_md_right"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>