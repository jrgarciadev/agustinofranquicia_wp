<section id="footer">
    <div class="container-fluid border_top_primary" id="footer_div">
        <div class="row footer_row">
            <div class="col">
                <div class="d-flex justify-content-center"><img src="<?php bloginfo('template_url')?>/img/logo_footer.gif" id="logo_footer" class="img-fluid"></div>
            </div>
        </div>
        <div class="row footer_row">
            <div class="col">
                <?php wp_nav_menu(array(
	'container' => false,
	'items_wrap' => '<ul class="list-inline" id ="menu-top">%3$s</ul>',
	'theme_location' => 'footer',
)
);?>
       </div>
   </div>
   <div class="row footer_row">
    <div class="col">
        <?php wp_nav_menu(array(
	'container' => false,
	'items_wrap' => '<ul class="list-inline" id ="menu-top">%3$s</ul>',
	'theme_location' => 'social-footer-icons',
)
);?>
</div>
</div>
</div>
<div class="d-flex justify-content-center align-items-center" id="footer_bottom">

    <div class = "row d-flex justify-content-center align-items-center" id="copyright_container">
        <div class="col-1 align-self-center" id="left_bottom_footer">
            <hr class="separator1">
        </div>
        <div class="col-10 col-lg-6 col-md-8 align-self-center" id="copyright">
            <p class="text-center text_white none_margin none_padding">Copyright 2018 Agustino - Todos los derechos reservados | Diseño Estudio Tanque&nbsp;</p>
        </div>
        <div class="col-1 align-self-center" id="right_bottom_footer">
            <hr class="separator1">
        </div>
    </div>

    <div id="to-fixed-top" class="btn btn-primary primary_button btn-up-page text-center to-top to-fixed">
        <a href="#hero" class="js-scroll-trigger">
            <i class="icon ion-android-arrow-up"></i>
        </a>
    </div>

    <div class="btn-up-page text-center btn btn-primary primary_button to-top" style="padding-top: 6px;">
        <a href="#hero" class="js-scroll-trigger">
            <i class="icon ion-android-arrow-up"></i>
        </a>
    </div>
</div>

</section>



<!-- Scripts =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= -->
<script src="<?php bloginfo('template_url')?>/js/jquery-3.2.1.min.js"></script>
<script src="<?php bloginfo('template_url')?>/js/TweenMax.min.js"></script>
<script src="<?php bloginfo('template_url')?>/js/ScrollToPlugin.min.js"></script>
<script src="<?php bloginfo('template_url')?>/js/jquery.waypoints.min.js"></script>
<script src="<?php bloginfo('template_url')?>/js/masonry.pkgd.min.js"></script>
<script src="<?php bloginfo('template_url')?>/js/underscore.min.js"></script>
<script src="<?php bloginfo('template_url')?>/js/main.js"></script>
<script src="<?php bloginfo('template_url')?>/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url')?>/js/bs-animation.js"></script>
<script src="<?php bloginfo('template_url')?>/js/jquery.slides.min.js"></script>
<script src="<?php bloginfo('template_url')?>/js/scrollreveal.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url')?>/js/move-top.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url')?>/js/easing.js"></script>
<script src="https://unpkg.com/@bootstrapstudio/bootstrap-better-nav/dist/bootstrap-better-nav.min.js"></script>
<script>
    $(function(){
        $("#slide_1").slidesjs({
            width: 356,
            navigation:false,
            pagination:false
        });
        $("#slide_2").slidesjs({
            width: 150,
            navigation:false,
            pagination:false
        });
    });
</script>