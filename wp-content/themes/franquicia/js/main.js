$(document).ready(function() {
	// console.clear();
	
	var $window = $(window);

	if (!("ontouchstart" in document.documentElement)) {
		document.documentElement.className += " no-touch";
	} else {
		document.documentElement.className += " touch";
	}
	
	/**=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	* ANIMATE.CSS
	*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
	$('.animated').not($('.translated')).waypoint(function(){
		var $element = $(this.element),
		animation = $element.data('animation') || 'fadeIn';
		
		$element.addClass(animation);
	}, {offset: '110%'});

	$('.translated').waypoint(function(){
		var $element = $(this.element),
		animation = $element.data('animation') || 'fadeIn';
		
		$element.addClass(animation);
	}, {offset: '100%'});

	/**=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	* SCROLL TO
	*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
	function ascrollto(id) {
		var etop = $(id).offset().top;
		$('html, body').animate({
			scrollTop: etop - 80
		}, 1000);
	}
	var siteurl = $('#siteurl');
	$('a[href^="#"]').on('click', function(e) {
		e.preventDefault();
		if (this.hash.length > 1 && $(this.hash).length) {
			ascrollto(this.hash);
		} 
		else if(this.hash.length > 1) {
			var link = siteurl.val()+'#'+this.hash.replace('#', '');
			window.location = link;
		}
	});

	/**=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	* HEADER STICK
	*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
	(function(){
		var didScroll;
		var lastScrollTop = 0;
		var delta = 5;
		var $header = $('#header');
		var $navbar = $('#navbar-header');
		var navbarHeight = $header.outerHeight();

		$('.navbar-toggler').on('click', function(){
			 $header.removeClass('nav-up').addClass('nav-down');
		});
		
		$window.scroll(function(event){ 
			didScroll = true;
			if ($window.scrollTop() > 80) {
				$header.addClass('stick');
				$navbar.addClass('stick');
			} else {
				$header.removeClass('stick');
				$navbar.removeClass('stick');
			}
		});

		setInterval(function() {
			if (didScroll) {
				hasScrolled();
				didScroll = false;
			}
		}, 250);

		function hasScrolled() {
			var st = $(this).scrollTop();
			
		    // Make scroll more than delta
		    if(Math.abs(lastScrollTop - st) <= delta)
		    	return;
		    
		    // If scrolled down and past the navbar, add class .nav-up.
		    if (st > lastScrollTop && st > navbarHeight){
		        // Scroll Down
		        $header.removeClass('nav-up').addClass('nav-down');
		    } else {
		        // Scroll Up
		        if(st + $window.height() < $(document).height()) {
		        	$header.removeClass('nav-down').addClass('nav-up');
		        }
		    }
		    lastScrollTop = st;
		}
	}());

	/**=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	* MENU MOBILE
	*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
	var $menu_responsive = $('#menu_mobile');
	$('.open-menu').on('click', function(e) {
		$menu_responsive.addClass('open');
		$window.off('mousewheel DOMMouseScroll');
		e.preventDefault();
	});
	$('.close-menu').on('click', function(e) {
		$menu_responsive.removeClass('open');
		createScrollTween();
		e.preventDefault();
	});

	var $submenu_list = $('.menu-responsive li.menu-item-has-children > a');
	$submenu_list.on('click', function(e) {
		e.preventDefault();
		var $this = $(this),
		$parent = $this.parent(),
		$submenu = $this.next();
		$('.menu-responsive li.menu-item-has-children').not($parent).removeClass('active');
		if ($parent.hasClass('active'))
			$submenu.slideUp();
		else
			$submenu.slideDown(); 
		$parent.toggleClass('active');

	});


	/**=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	* SMOOTH SCROLL
	*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
	var scrollTween = false;

	function createScrollTween() {
		var scrollTime = 1; //Scroll time
		var scrollDistance = 300; //Distance. Use smaller value for shorter scroll and greater value for longer scroll

		$window.on("mousewheel DOMMouseScroll", function(event){
			event.preventDefault();	
			
			var delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3;
			var scrollTop = $window.scrollTop();
			var finalScroll = scrollTop - parseInt(delta*scrollDistance);
			
			scrollTween = TweenMax.to($window, scrollTime, {
				scrollTo : { y: finalScroll, autoKill:true },
					ease: Power2.easeOut,	//For more easing functions see http://api.greensock.com/js/com/greensock/easing/package-detail.html
					autoKill: true,
					overwrite: 5							
				});
		});
	}
	createScrollTween();


	/**=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	* TO TOP
	*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
	var $toTop = $('.to-top');
	$toTop.on('click', function(e) {
		e.preventDefault();
		$("html, body").animate({scrollTop: 0}, 400);
	});
	
	function toTop() {
		if($window.scrollTop() > 800 && $window.scrollTop() < $('#contact_form_div').offset().top - 800) {
			$('#to-fixed-top').addClass('stick');
		} else {
			$('#to-fixed-top').removeClass('stick');
		}
	}
	toTop();
	$window.scroll(toTop);

	/**=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	* UBICACION CAROUSEL
	*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
	/**=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	* NUESTROS SERVICIOS
	*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
	$('.our-services-wrapper .image-central img').on('click', function() {
		var $element = $($(this).attr('data-q'));
		$('.our-service-item').removeClass('overlay-fixed');
		$element.toggleClass('overlay-fixed');
	});
	/**=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	* TESTIMONIOS
	*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
	// $('.service-testimonies').each(function() {
	// 	var $this = $(this);
	// 	$this.find('.testimonies-list').slick({
	// 		infinite: true,
	// 		slidesToShow: 1,
	// 		slidesToScroll: 1,
	// 		fade: true,
	// 		autoplay: true,
	// 		autoplaySpeed: 4000,
	// 		arrows: true,
	// 		prevArrow: $this.find('.arrow-left'),
	// 		nextArrow: $this.find('.arrow-right'),
	// 		dots: false,
	// 		draggable: true,
	// 		focusOnSelect: false
	// 	});
	// });

	/**=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	* MAIN SLIDER
	*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
	// $('.main-slider.slider-module .slider-content').slick({
	// 	infinite: true,
	// 	slidesToShow: 1,
	// 	slidesToScroll: 1,
	// 	fade: true,
	// 	autoplay: true,
	// 	autoplaySpeed: 6000,
	// 	arrows: true,
	// 	prevArrow: $('.control.control-left'),
	// 	nextArrow: $('.control.control-right'),
	// 	dots: false,
	// 	draggable: true,
	// 	focusOnSelect: false
	// });


	$('video[autoplay]').each(function() {
		this.play();
	});


	$('.play-video').on('click', function() {
		var $this = $(this);
		var $video = $this.next();

		if ($this.hasClass('active')) {
			$video[0].pause();
		} else {
			$video[0].play();
		}

		$this.toggleClass('active');
	});

	/**=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	* FAQ
	*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
	$('.faq-accordion.multiple .faq-title').on('click', function(e) {
		$(this).toggleClass('active').next().slideToggle(300);
	});

	$('.faq-accordion.singular .faq-title').on('click', function(e) {
		var $this = $(this),
		$next = $this.next(),
		$parent = $this.closest('.faq-accordion');

		$parent.find('.faq-title').not($this).removeClass('active');
		$parent.find('.faq-content').not($next).slideUp();
		
		$this.toggleClass('active');
		$next.slideToggle(300);

		e.preventDefault();
	});

	/**=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	* CONTACTO
	*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
	$('.input, .textarea').on('focus', function() {
		$(this).closest('.field-group').addClass('active');	
	});
	$('.input, .textarea').on('blur', function() {
		$(this).closest('.field-group').removeClass('active');	
	});
	$(".textarea").keyup(function(e) {
		if(e.keyCode == 13) {
			var rowCount = $(this).attr("rows");
			$(this).attr({rows: parseInt(rowCount) + 2});
		}
	});

	/**=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	* CAROUSEL
	*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
	// $('.carousel-wrapper').each(function() {
	// 	var $this = $(this);
	// 	$this.find('.carousel-list').slick({
	// 		infinite: true,
	// 		slidesToShow: 1,
	// 		slidesToScroll: 1,
	// 		fade: true,
	// 		autoplay: true,
	// 		autoplaySpeed: 4000,
	// 		arrows: false,
	// 		dots: true,
	// 		adaptiveHeight: true,
	// 		appendDots: $this.find('.carousel-dots'),
	// 		draggable: true,
	// 		focusOnSelect: false
	// 	});
	// });


	/**=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	* NUESTRO NEGOCIO
	*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
	$('.our-service-item').on('click', function() {
		$(this).toggleClass('overlay-fixed');
	});
	var masonry_options = {
		// options
		percentPosition: true,
		itemSelector: '.service-masonry',
		columnWidth: '.service-masonry',
		horizontalOrder: true,
		originLeft: true
	}
	var $masonry = $('.masonry-grid').masonry(masonry_options);

	function checkmasonry(){
		if ($window.outerWidth() < 1080) { $masonry.masonry('destroy'); } else { $masonry.masonry(masonry_options); }
	}

	checkmasonry();
	$window.on('resize', _.debounce(checkmasonry, 400));

	/**=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	* PLANES
	*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
	$('.plan-dropdown').on('click', '.expand-plan', function(e) {
		e.preventDefault();
		var item = $(this).closest('.plan-item');
		var content = item.find('.plan-content')

		item.toggleClass('collapsed expanded');
		$('.plan-content').not(content).slideUp();
		content.slideToggle();
	});

});