<section id="hero" class="contact_form_icon text_secondary">
    <header id="header" class="header-wrapper">
        <nav id = "navbar-header" class="navbar navbar-dark navbar-expand-md better-bootstrap-nav-left">
            <div class="container-fluid"><a class="navbar-brand" href="#" id="navbar_brand">| BIENVENIDOS</a><button class="navbar-toggler" data-toggle="collapse" data-target="#primary-menu"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                <?php
wp_nav_menu(array(
	'theme_location' => 'primary',
	'container' => 'div',
	'container_id' => 'primary-menu',
	'container_class' => 'collapse navbar-collapse justify-content-center',
	'menu_id' => false,
	'menu_class' => 'navbar-nav',
	'depth' => 3,
	'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
	'walker' => new wp_bootstrap_navwalker(),
));
?>

               <?php
wp_nav_menu(array(
	'theme_location' => 'social-icons',
	'container' => 'div',
	'container_id' => 'main-nav',
	'container_class' => 'collapse navbar-collapse right-nav',
	'menu_id' => false,
	'menu_class' => 'navbar-nav',
	'depth' => 3,
	'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
	'walker' => new wp_bootstrap_navwalker(),
));
?>
           </div>
       </nav>
   </header>
   <div id="hero-logo">
    <div class="container">
        <div class="row">
            <div class="col"><img src="<?php bloginfo('template_url')?>/img/logo@2x.png" id="logo"></div>
        </div>
        <div class="row" id="row-arrow">
            <div class="col d-flex flex-row justify-content-center align-items-center"><i class="fa fa-angle-down icon-arrow-down down_arrow_icon" data-bs-hover-animate="rubberBand" id="down_arrow_icon"></i></div>
        </div>
    </div>
</div>
</section>
