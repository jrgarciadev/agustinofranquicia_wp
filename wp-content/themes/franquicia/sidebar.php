<?php
/**
 * El sidebar contiene todas las secciones del tema
 *
 * @package WordPress
 * @subpackage Franquicia 
 * @since Franquicia 1.0
 * @author Junior Garcia <www.jrdeveloper.com.ve> jrgarciadev@gmail.com
 */


get_template_part( 'section', 'hero' );

get_template_part( 'section', 'about_us' );

get_template_part( 'section', 'movie' );

get_template_part( 'section', 'agustino_shop' );

get_template_part( 'section', 'local' );

get_template_part( 'section', 'locale' );

get_template_part( 'section', 'contact' );

?>
