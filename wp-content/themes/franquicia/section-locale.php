    <section id="locale">
        <div class="container-fluid secondary_color_block" id="location_div">
            <div id="location_decoration" class="square_decoration"></div>
            <div class="row">
                <div class="col-12 col-lg-6 col-md-6">
                    <div id="location_block_text" class="block_primary_text">
                        <?php if (have_posts()): while (have_posts()): the_post();?>
		                            <?php if (get_post_field('post_name', get_post()) == 'slug-6'): ?>
		                                <p class='text_primary top_primary_text'><?php the_title();?></p>
		                                <?php the_content();?>
		                            <?php endif;?>
	                        <?php endwhile;endif;?>
                        <a class="btn btn-primary primary_button" href='#contact_form_div' role="button" id="about_us_button">| quieres franquicia?</a></div>
                    </div>
                    <div class="col-12 col-lg-6 col-md-6" id="localtions_slide">
                        <div id="location" class="container">
                            <div class="row">
                                <div id="slide_2" style = "width: 300px; height: 300px" class="col-12">
                                    <!-- PREV -->
                                    <a href="#" class="slidesjs-previous  slidesjs-navigation"><i class="icon-arrow-left text_white arrow_location"></i></i></a>

                                    <div id="location_right_square" class="square_decoration container">
                                        <h4 class="text_white location_slide_title">Argentina</h4>
                                        <div class="container-fluid location_slide_content">
                                            <div class="row">
                                                <div class="col-12 col-lg-6 none_margin none_padding col_location_info">
                                                    <div>
                                                        <p class="text_white text_paragraph_sm none_padding none_margin">Buenos Aires / Capital</p>
                                                        <p class="text_white text_paragraph_xs none_margin">Av. Santa Fé #1549<i class="material-icons arrow_right_xs text_primary pull-left">keyboard_arrow_right</i></p>
                                                        <p class="text_white text_paragraph_xs none_margin">Galerias Pacifico<i class="material-icons arrow_right_xs text_primary pull-left">keyboard_arrow_right</i></p>
                                                        <p id="dot_mobile_locations_separator" class="text_white text_paragraph_xs none_margin text_primary">&nbsp;-----------</p>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-lg-6 none_margin none_padding col_location_info">
                                                    <div>
                                                        <p class="text_white text_paragraph_sm none_padding none_margin">Atención<br></p>
                                                        <p class="text_white text_paragraph_xs none_margin">Lun - Sá / 11 a 20:30hrs.<i class="material-icons arrow_right_xs text_primary pull-left">keyboard_arrow_right</i></p>
                                                        <p class="text_white text_paragraph_xs none_margin">Do / 13 a 20:30hrs.<i class="material-icons arrow_right_xs text_primary pull-left">keyboard_arrow_right</i></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="location_right_square" class="square_decoration container">
                                        <h4 class="text_white location_slide_title">Argentina</h4>
                                        <div class="container-fluid location_slide_content">
                                            <div class="row">
                                                <div class="col-12 col-lg-6 none_margin none_padding col_location_info">
                                                    <div>
                                                        <p class="text_white text_paragraph_sm none_padding none_margin">Buenos Aires / Capital</p>
                                                        <p class="text_white text_paragraph_xs none_margin">Av. Santa Fé #1549<i class="material-icons arrow_right_xs text_primary pull-left">keyboard_arrow_right</i></p>
                                                        <p class="text_white text_paragraph_xs none_margin">Galerias Pacifico<i class="material-icons arrow_right_xs text_primary pull-left">keyboard_arrow_right</i></p>
                                                        <p id="dot_mobile_locations_separator" class="text_white text_paragraph_xs none_margin text_primary">&nbsp;-----------</p>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-lg-6 none_margin none_padding col_location_info">
                                                    <div>
                                                        <p class="text_white text_paragraph_sm none_padding none_margin">Atención<br></p>
                                                        <p class="text_white text_paragraph_xs none_margin">Lun - Sá / 11 a 20:30hrs.<i class="material-icons arrow_right_xs text_primary pull-left">keyboard_arrow_right</i></p>
                                                        <p class="text_white text_paragraph_xs none_margin">Do / 13 a 20:30hrs.<i class="material-icons arrow_right_xs text_primary pull-left">keyboard_arrow_right</i></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- NEXT -->
                                    <a href="#" class="slidesjs-next slidesjs-navigation"><i class="icon-arrow-right text_white arrow_location"></i></a>
                                </div>

<!--

                            <div id= "slide_2" class="col-9 d-flex justify-content-center align-items-center col-lg-9 col-md-9">
                                <div id="location_right_square" class="square_decoration container">
                                    <h4 class="text_white location_slide_title">Argentina</h4>
                                    <div class="container-fluid location_slide_content">
                                        <div class="row">
                                            <div class="col-12 col-lg-6 none_margin none_padding col_location_info">
                                                <div>
                                                    <p class="text_white text_paragraph_sm none_padding none_margin">Buenos Aires / Capital</p>
                                                    <p class="text_white text_paragraph_xs none_margin">Av. Santa Fé #1549<i class="material-icons arrow_right_xs text_primary pull-left">keyboard_arrow_right</i></p>
                                                    <p class="text_white text_paragraph_xs none_margin">Galerias Pacifico<i class="material-icons arrow_right_xs text_primary pull-left">keyboard_arrow_right</i></p>
                                                    <p id="dot_mobile_locations_separator" class="text_white text_paragraph_xs none_margin text_primary">&nbsp;-----------</p>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-6 none_margin none_padding col_location_info">
                                                <div>
                                                    <p class="text_white text_paragraph_sm none_padding none_margin">Atención<br></p>
                                                    <p class="text_white text_paragraph_xs none_margin">Lun - Sá / 11 a 20:30hrs.<i class="material-icons arrow_right_xs text_primary pull-left">keyboard_arrow_right</i></p>
                                                    <p class="text_white text_paragraph_xs none_margin">Do / 13 a 20:30hrs.<i class="material-icons arrow_right_xs text_primary pull-left">keyboard_arrow_right</i></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>