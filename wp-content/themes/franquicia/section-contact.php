<section id="contact">
    <div id="contact_div" class="d-flex justify-content-center align-items-center">
        <div id="contact_form_div" class="d-flex justify-content-center align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col order-2 col-lg-6 col-md-6 col-sm-12 col-xs-12 order-lg-1 order-xs-2" id="contact_form_col">
                        <div id="contact_form" class="square_decoration d-flex justify-content-center
                        ">
                            <?php get_template_part( 'contact-widget' ); ?>
                        </div>
                    </div>
                    <div class="col order-1 col-lg-6 col-md-6 col-sm-12 col-xs-12 order-lg-2 order-xs-1" id="contact_form_col_right">
                        <div id="contact_right_text_div">
                         <?php if (have_posts()) : while (have_posts()) : the_post();?>
                            <?php if(get_post_field( 'post_name', get_post() ) =='slug-7') : ?>
                                <p class='text_default text_secondary_title'><?php the_title(); ?></p>
                                <?php the_content(); ?>
                            <?php endif; ?>
                        <?php endwhile; endif; ?>

                        <div id="contact_left_info">

                           <?php if (have_posts()) : while (have_posts()) : the_post();?>
                            <?php if(get_post_field( 'post_name', get_post() ) =='slug-8') : ?>
                                <?php the_content(); ?>
                            <?php endif; ?>
                        <?php endwhile; endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="contact_div_img">
    <img class="img-fluid" src="<?php bloginfo('template_url')?>/img/AgustinoCl-Franquicia-Contacto.gif" id="movie_bkg_img">
    <img class="img-fluid" src="<?php bloginfo('template_url')?>/img/AgustinoCl-Franquicia-Contacto-mobile.gif" id="movie_bkg_img_mobile">
</div>
</div>
</section>