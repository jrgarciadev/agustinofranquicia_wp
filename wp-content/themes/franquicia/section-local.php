    <section id="local">
        <div class="container">
            <div class="row d-flex align-items-start">
                <div class="col-12 col-lg-7 col-md-7">
                    <div class="text_block_with_list">
                       <?php if (have_posts()) : while (have_posts()) : the_post();?>
                        <?php if(get_post_field( 'post_name', get_post() ) =='slug-5') : ?>
                         <h1 class="text_secondary heading_secondary_2 none_margin"><?php the_title(); ?></h1>
                         <?php the_content(); ?>
                     <?php endif; ?>
                 <?php endwhile; endif; ?>
                <div class="d-flex justify-content-center align-items-center"><a href="#"><i class="fa fa-angle-down down_arrow_icon icon-arrow-down" data-bs-hover-animate="rubberBand" id="down_arrow_icon2"></i></a></div>
            </div>
        </div>
        <div class="col-12 col-lg-5 col-md-5 col-sm-12" id="local_right_column">
            <div id="agustino_local_img"><img src="<?php bloginfo('template_url')?>/img/AgustinoCL-Franquicia-4.gif" id="local_img" class="border_white img-fluid"></div>
        </div>
    </div>
</div>
</section>