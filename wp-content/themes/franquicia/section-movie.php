<section id="movie">
    <div id="movie_div" class="d-flex justify-content-center align-items-center">
        <div id="video_div" class="d-flex justify-content-center align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col d-flex justify-content-center" id="col_movie_link"><span id="movie_link" class="d-flex justify-content-center align-items-center"><i class="fa fa-play text_primary" id="movie_play_icon"></i></span></div>
                </div>
                <div class="row">
                    <div class="col">
                        <h4 id="movie_text" class="text_white">Ver Video</h4>
                    </div>
                </div>
            </div>
        </div>
        <div id="video_div_img">
            <img class ="img-fluid"  src="<?php bloginfo('template_url')?>/img/AgustinoCl-Franquicia-Video@2x.gif" id="movie_bkg_img">
            <img class="img-fluid" src="<?php bloginfo('template_url')?>/img/AgustinoCl-Franquicia-Video_mobile.gif" id="movie_bkg_img_mobile">
        </div>
    </div>
</section>