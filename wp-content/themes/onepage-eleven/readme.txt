=== Onepage Eleven ===

Contributors: evidisha
 Tags: right-sidebar, custom-menu, custom-logo, editor-style, featured-images, translation-ready, flexible-header, sticky-post, theme-options, portfolio
Requires at least: 4.0.5
Tested up to: 4.5.2
Stable tag: 1.0.8
== Theme License & Copyright ==
 Onepage Eleven is distributed under the terms of the GNU GPL
 Onepage Eleven Copyright 2018  Onepage Eleven, evidisha.com
License : GPL License
License URL: https://www.gnu.org/licenses/gpl-3.0.en.html

"Onepage Eleven theme One Page Business WordPress Theme" By evidisha.


== Description ==
Onepage Eleven is a child theme of featuredlite it is loaded with overall functionality which is sufficient for one to create site.

== Theme License & Copyright ==
Onepage Eleven, Copyright 2018 Onepage Eleven Theme , evidisha.com
Onepage Eleven Theme is licensed under the GPL3.

WordPress theme "Onepage Eleven" is a child theme of "featuredlite".
featuredlite Theme is licensed under the GPL3.

License for images:
1. Image Name: main.jpeg 
Resource link: https://www.pexels.com/photo/vintage-hipster-chrome-design-92633/
Licensed under the CCO license.
License link : https://www.pexels.com/photo-license/

== FontAwesome License ==
License: SIL OFL 1.1
License URl for Font : http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL

== 1.0.2  =
* Extra file removed

== 1.0.1  =
* Initial release

== 1.0.0  =
* Initial release

== Theme License & Copyright ==
Onepage Eleven is distributed under the terms of the GNU GPL
Onepage Eleven -Copyright 2018 Onepage Eleven, evidisha.com
Once again, thank you so much for trying the Onepage Eleven WordPress Theme. As we said at the beginning, we will be glad to help you. If you have any questions related to this theme then do let us know & we will try our best to assist. If you have any general questions related to the themes on evidisha, we would recommend you to visit the evidisha Support Forum and ask your queries there.