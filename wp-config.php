<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'franquicia');

/** MySQL database username */
define('DB_USER', 'juniorgarcia');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '`)40[{>z;$j`%j-]_&b?q94hX~_SUptnQbVN6wOiL2=~bln|?qF>rviy~evoh]h~');
define('SECURE_AUTH_KEY',  'dID7gkAu]2XARO)p#lm_MLdYD>PyKzOjF9=71&r6L/(z+,=2Q;vHn#?GOdx{Lz{s');
define('LOGGED_IN_KEY',    '%~khGs)<0iZv30LDt#/IbAQnAh*6Z{+3JB&PjA2j83N?=jEPRR=cYV-.U7K&UX.;');
define('NONCE_KEY',        'dn2&L^f!TgL>uuC^I8w-8v-=0NT~.=eC^pdoc%h?|iXi$Rnq=i26vWW*~7JCjsnu');
define('AUTH_SALT',        'yO9=~m*Pg2i;t,<XZH^e!%9HD?Pwax#4%jklY0,vxK6|N&>1&yD$u(Ie3#~;~/T=');
define('SECURE_AUTH_SALT', 'opF=.er{,&8oB,oYVz<Ydu4]A0CN|I(D~i~CE:h#H~QU|xC`Js$#@_Q.lfR Rb,P');
define('LOGGED_IN_SALT',   't5ewYXHqrj4!>8_Rev7m5<m]s_,vCa.[[j5.]#t;#79p)XF%AoOMor$7[cd<#A3u');
define('NONCE_SALT',       '}bJ)NePP2uF7@<E/hOw$V&3X.LHHj(}>z@0A}#u6GZK#fjmCL].E?5VZ]ixA>P1N');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
